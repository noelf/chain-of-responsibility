package ch.bbw;

import ch.bbw.Chain.BugHandler;
import ch.bbw.Chain.ExceptionHandler;
import ch.bbw.Chain.InfoHandler;

import java.util.logging.Level;

public class LogHandlerChain {
    private LogChain l1;

    private LogHandlerChain() {
        this.l1 = new BugHandler();
        LogChain l2 = new ExceptionHandler();
        LogChain l3 = new InfoHandler();
        l1.setNextChain(l2);
        l2.setNextChain(l3);
    }

    public static void main(String[] args) {
        LogHandlerChain logHandlerChain = new LogHandlerChain();
        Level logLevel = Level.INFO;
        logHandlerChain.l1.evaluation(logLevel);
    }
}
