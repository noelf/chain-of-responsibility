package ch.bbw;

import java.util.logging.Level;

public interface LogChain {
    void setNextChain(LogChain nextChain);

    void evaluation(Level logLevel);
}
