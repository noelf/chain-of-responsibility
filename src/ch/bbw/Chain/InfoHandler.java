package ch.bbw.Chain;

import ch.bbw.LogChain;

import java.util.logging.Level;

public class InfoHandler implements LogChain {
    private LogChain chain;

    @Override
    public void setNextChain(LogChain nextChain) {
        this.chain = nextChain;
    }

    @Override
    public void evaluation(Level logLevel) {
        if (logLevel == Level.INFO) {
            System.out.println("INFO logger");
        } else {
            this.chain.evaluation(logLevel);
        }
    }
}
