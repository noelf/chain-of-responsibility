package ch.bbw.Chain;

import ch.bbw.LogChain;

import java.util.logging.Level;

public class BugHandler implements LogChain {
    private LogChain chain;

    @Override
    public void setNextChain(LogChain nextChain) {
        this.chain = nextChain;
    }

    @Override
    public void evaluation(Level logLevel) {
        if (logLevel == Level.SEVERE) {
            System.out.println("SEVERE bugs found");
        } else {
            this.chain.evaluation(logLevel);
        }
    }
}
