package ch.bbw.Chain;

import ch.bbw.LogChain;

import java.util.logging.Level;

public class ExceptionHandler implements LogChain {
    private LogChain chain;

    @Override
    public void setNextChain(LogChain nextChain) {
        this.chain = nextChain;
    }

    @Override
    public void evaluation(Level logLevel) {
        if (logLevel == Level.WARNING) {
            System.out.println("WARNING exception found");
        } else {
            this.chain.evaluation(logLevel);
        }
    }
}
